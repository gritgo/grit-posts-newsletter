<?php

namespace Gpn;

use Grit\Plugin;

class GritPostsNewsletter extends Plugin 
{
	protected $load_carbon = false;

	protected $load_i18n = false;

	protected $update_url = "https://demo.gresak.net/grit-posts-newsletter";
}